package info.dhickcunk.kasirku.service;

import java.util.ArrayList;

import info.dhickcunk.kasirku.model.Barang;
import info.dhickcunk.kasirku.model.Login;
import info.dhickcunk.kasirku.model.Transaksi;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIService {

    //Login
    @FormUrlEncoded
    @POST("user.php?action=login")
    Call<Login> login(@Field("username") String username, @Field("password") String password);

    //Get All Barang
    @GET("barang.php?action=get_all")
    Call<Barang> getAllBarang();

    //Get Barang By Barcode
    @GET("barang.php?action=get_by_id")
    Call<Barang> getBarangById(@Query("kode_barcode") String kodeBarang);

    // Search Barang
    @GET("barang.php?action=search")
    Call<Barang> searchBarang(@Query("query") String searchQuery);

    //Edit Barang
    @FormUrlEncoded
    @POST("barang.php?action=edit_barang")
    Call<Barang> editBarang(@Field("id_barang") String idBarang, @Field("harga") String harga, @Field("stok") String stok);

    // Get All Transaksi
    @GET("transaksi.php?action=get_all")
    Call<Transaksi> getAllTransaksi();

    //Get Transaksi By ID Kasir
    @GET("transaksi.php?action=get_by_kasir")
    Call<Transaksi> getTransaksiByKasir(@Query("id_kasir") int id_kasir);

    // Search Transaksi All
    @GET("transaksi.php?action=search_all")
    Call<Transaksi> searchTransaksiAll(@Query("query") String searchQuery);

    // Search Transaksi By ID Kasir
    @GET("transaksi.php?action=search_by_kasir")
    Call<Transaksi> searchByKasir(@Query("id_kasir") int id_kasir, @Query("query") String searchQuery);

    // Add Transaksi
    @FormUrlEncoded
    @POST("transaksi.php?action=insert_transaksi")
    Call<Transaksi> insertTransaksi(
            @Field("tgl_transaksi") String tglTransaksi,
            @Field("total_harga") String totalHarga,
            @Field("id_user") String idUser,
            @Field("id_barang[]") ArrayList<Integer> idBarang
    );


}
