
package info.dhickcunk.kasirku.model;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataTransaksi implements Serializable {

    @SerializedName("id_transaksi")
    @Expose
    private String idTransaksi;
    @SerializedName("kode_transaksi")
    @Expose
    private String kodeTransaksi;
    @SerializedName("tgl_transaksi")
    @Expose
    private String tglTransaksi;
    @SerializedName("total_harga")
    @Expose
    private String totalHarga;
    @SerializedName("user")
    @Expose
    private DataUser user;
    @SerializedName("barang")
    @Expose
    private List<DataBarang> barang = null;

    public String getIdTransaksi() {
        return idTransaksi;
    }

    public void setIdTransaksi(String idTransaksi) {
        this.idTransaksi = idTransaksi;
    }

    public String getKodeTransaksi() {
        return kodeTransaksi;
    }

    public void setKodeTransaksi(String kodeTransaksi) {
        this.kodeTransaksi = kodeTransaksi;
    }

    public String getTglTransaksi() {
        return tglTransaksi;
    }

    public void setTglTransaksi(String tglTransaksi) {
        this.tglTransaksi = tglTransaksi;
    }

    public String getTotalHarga() {
        return totalHarga;
    }

    public void setTotalHarga(String totalHarga) {
        this.totalHarga = totalHarga;
    }

    public DataUser getUser() {
        return user;
    }

    public void setUser(DataUser user) {
        this.user = user;
    }

    public List<DataBarang> getBarang() {
        return barang;
    }

    public void setBarang(List<DataBarang> barang) {
        this.barang = barang;
    }
}
