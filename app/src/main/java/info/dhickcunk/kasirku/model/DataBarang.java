package info.dhickcunk.kasirku.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DataBarang implements Serializable {
    @SerializedName("id_barang")
    @Expose
    private String idBarang;
    @SerializedName("kode_barcode")
    @Expose
    private String kodeBarcode;
    @SerializedName("nama_barang")
    @Expose
    private String namaBarang;
    @SerializedName("harga")
    @Expose
    private String harga;
    @SerializedName("stok")
    @Expose
    private String stok;
    @SerializedName("foto")
    @Expose
    private String foto;

    public String getIdBarang() {
        return idBarang;
    }

    public void setIdBarang(String idBarang) {
        this.idBarang = idBarang;
    }

    public String getKodeBarcode() {
        return kodeBarcode;
    }

    public void setKodeBarcode(String kodeBarcode) {
        this.kodeBarcode = kodeBarcode;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getStok() {
        return stok;
    }

    public void setStok(String stok) {
        this.stok = stok;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}