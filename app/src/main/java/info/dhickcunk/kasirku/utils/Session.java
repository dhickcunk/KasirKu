package info.dhickcunk.kasirku.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import info.dhickcunk.kasirku.model.DataUser;

public class Session {
	public static SharedPreferences pref;
	public static Editor editor;
	public static String PREF_NAME = "KasirKu";

	public static void createLoginSession(Context context, DataUser dataUser) {
		pref = context.getSharedPreferences(PREF_NAME, 0);
		editor = pref.edit();

		editor.putString("idUser", dataUser.getIdUser());
		editor.putString("username", dataUser.getUsername());
		editor.putString("password", dataUser.getPassword());
		editor.putString("namaUser", dataUser.getNamaUser());
		editor.putString("hakAkses", dataUser.getHakAkses());
		editor.putString("fotoUrl", dataUser.getFoto());

		editor.commit();
	}

	public static void logout(Context context) {
		pref = context.getSharedPreferences(PREF_NAME, 0);
		editor = pref.edit();
		editor.clear();
		editor.commit();
	}
}