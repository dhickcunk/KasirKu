package info.dhickcunk.kasirku.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import info.dhickcunk.kasirku.R;
import info.dhickcunk.kasirku.activity.BarangActivity;
import info.dhickcunk.kasirku.model.Barang;
import info.dhickcunk.kasirku.model.DataBarang;
import info.dhickcunk.kasirku.service.APIService;
import info.dhickcunk.kasirku.service.KasirkuApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;

public class BarangLengkapAdapter extends RecyclerView.Adapter<BarangLengkapAdapter.ViewHolder> {

    private List<DataBarang> barangList;
    private BarangActivity activity;
    View viewRoot;
    Dialog dialogEdit;
    APIService apiService;

    public BarangLengkapAdapter(BarangActivity activity, List<DataBarang> barangList) {
        this.barangList = barangList;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_barang_lengkap, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.textNamaBarang.setText(barangList.get(position).getNamaBarang());

        double amountDouble = Double.parseDouble(barangList.get(position).getHarga());

        viewHolder.textHargaBarang.setText("Rp. " + String.format("%,.0f", amountDouble));
        viewHolder.textStokBarang.setText("Stok : " + barangList.get(position).getStok());
        viewHolder.textBarcodeBarang.setText(barangList.get(position).getKodeBarcode());
        Glide.with(activity)
                .load(barangList.get(position).getFoto())
                .into(viewHolder.fotoBarang);
    }

    @Override
    public int getItemCount() {
        return barangList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textNamaBarang;
        TextView textStokBarang;
        TextView textHargaBarang;
        TextView textBarcodeBarang;
        ImageView fotoBarang;
        ImageButton imageButton;

        public ViewHolder(View view) {
            super(view);
            apiService = KasirkuApi.getClient().create(APIService.class);
            textNamaBarang = (TextView) view.findViewById(R.id.namaBarangText);
            textHargaBarang = (TextView) view.findViewById(R.id.hargaBarangText);
            textStokBarang = (TextView) view.findViewById(R.id.stokBarangText);
            textBarcodeBarang= (TextView) view.findViewById(R.id.kodeBarcodeText);
            fotoBarang = (ImageView)view.findViewById(R.id.fotoBarang);
            imageButton = (ImageButton) view.findViewById(R.id.menuEditButton);
            imageButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == imageButton.getId()){

                PopupMenu popupMenu = new PopupMenu(activity, imageButton);
                popupMenu.getMenuInflater().inflate(R.menu.edit_menu, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        dialogEdit = new Dialog(activity);
                        dialogEdit.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialogEdit.setContentView(R.layout.dialog_edit_barang);
                        dialogEdit.setCancelable(false);
                        dialogEdit.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialogEdit.findViewById(R.id.imageClose).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogEdit.dismiss();
                            }
                        });

                        final TextView judulDialog = (TextView) dialogEdit.findViewById(R.id.judulEdit);
                        final EditText editHarga = (EditText) dialogEdit.findViewById(R.id.editHarga);
                        final EditText editStok = (EditText) dialogEdit.findViewById(R.id.editStok);

                        judulDialog.setText(barangList.get(getAdapterPosition()).getNamaBarang());
                        editHarga.setText(barangList.get(getAdapterPosition()).getHarga());
                        editStok.setText(barangList.get(getAdapterPosition()).getStok());

                        dialogEdit.findViewById(R.id.buttonSave).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!editHarga.getText().toString().isEmpty() ||
                                    !editHarga.getText().toString().equalsIgnoreCase("") ||
                                    !editStok.getText().toString().isEmpty() ||
                                    !editStok.getText().toString().equalsIgnoreCase("")){

                                    editBarang(barangList.get(getAdapterPosition()).getIdBarang(), editHarga.getText().toString(), editStok.getText().toString());
                                }else{

                                }
                            }
                        });

                        dialogEdit.show();

//                        Toast.makeText(activity, "ITEM PRESSED = " + item.getTitle(), Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });
                popupMenu.show();

            }
        }
    }

    public void editBarang(String idBarang, String harga, String stok) {
        apiService.editBarang(idBarang, harga, stok).enqueue(new Callback<Barang>() {
            @Override
            public void onResponse(Call<Barang> call, Response<Barang> response) {
                Barang barang = response.body();
                if (barang.getSuccess().equalsIgnoreCase("1")){
                    Toast.makeText(activity, barang.getMessage(), Toast.LENGTH_SHORT).show();
                    dialogEdit.dismiss();
                    activity.getAllBarang();
                }else{
                    Toast.makeText(activity, barang.getMessage(), Toast.LENGTH_SHORT).show();
                    dialogEdit.dismiss();
                    activity.getAllBarang();
                }
            }

            @Override
            public void onFailure(Call<Barang> call, Throwable t) {
                Toast.makeText(activity, ""+t, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
