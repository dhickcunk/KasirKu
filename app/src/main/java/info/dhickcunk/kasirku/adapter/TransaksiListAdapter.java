package info.dhickcunk.kasirku.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import info.dhickcunk.kasirku.R;
import info.dhickcunk.kasirku.activity.DetailTransaksiActivity;
import info.dhickcunk.kasirku.model.DataTransaksi;

public class TransaksiListAdapter extends RecyclerView.Adapter<TransaksiListAdapter.ViewHolder> {
    List<DataTransaksi> transaksiList;
    Activity activity;

    public TransaksiListAdapter(Activity activity, List<DataTransaksi> transaksiList) {
        this.transaksiList = transaksiList;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_transaksi, viewGroup, false);
        return new TransaksiListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.namaKasirText.setText(transaksiList.get(position).getUser().getNamaUser());
        viewHolder.tanggalTransaksiText.setText(transaksiList.get(position).getTglTransaksi());
        viewHolder.kodeTransaksiText.setText(transaksiList.get(position).getKodeTransaksi());

        double amountDouble = Double.parseDouble(transaksiList.get(position).getTotalHarga());

        viewHolder.totalHargaText.setText("Rp. " + String.format("%,.0f", amountDouble));
        viewHolder.totalBarangText.setText(""+transaksiList.get(position).getBarang().size());
    }

    @Override
    public int getItemCount() {
        return transaksiList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView namaKasirText;
        TextView tanggalTransaksiText;
        TextView kodeTransaksiText;
        TextView totalHargaText;
        TextView totalBarangText;

        public ViewHolder(View view) {
            super(view);

            namaKasirText = (TextView) view.findViewById(R.id.namaKasirText);
            tanggalTransaksiText = (TextView) view.findViewById(R.id.tanggalTransaksiText);
            kodeTransaksiText = (TextView) view.findViewById(R.id.kodeTransaksiText);
            totalHargaText = (TextView) view.findViewById(R.id.totalHargaText);
            totalBarangText = (TextView) view.findViewById(R.id.totalBarangText);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(activity, DetailTransaksiActivity.class);
            intent.putExtra("dataTransaksi", transaksiList.get(getAdapterPosition()));
            activity.startActivity(intent);
        }
    }
}
