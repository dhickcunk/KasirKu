package info.dhickcunk.kasirku.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import info.dhickcunk.kasirku.R;
import info.dhickcunk.kasirku.activity.TambahTransaksiActivity;
import info.dhickcunk.kasirku.model.DataBarang;

public class BarangListAdapter extends RecyclerView.Adapter<BarangListAdapter.ViewHolder> {

    private List<DataBarang> barangList;
    private Activity activity;

    public BarangListAdapter(List<DataBarang> barangList, Activity activity) {
        this.barangList = barangList;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_barang, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.textNamaBarang.setText(barangList.get(position).getNamaBarang());

        double amountDouble = Double.parseDouble(barangList.get(position).getHarga());

        viewHolder.textHargaBarang.setText("Rp. " + String.format("%,.0f", amountDouble));
        viewHolder.textNomerBarang.setText(position+1+"");
    }

    @Override
    public int getItemCount() {
        return barangList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textNamaBarang;
        TextView textNomerBarang;
        TextView textHargaBarang;

        public ViewHolder(View view) {
            super(view);
            textNomerBarang = (TextView) view.findViewById(R.id.nomerBarangText);
            textNamaBarang = (TextView) view.findViewById(R.id.namaBarangText);
            textHargaBarang = (TextView) view.findViewById(R.id.hargaBarangText);
        }
    }
}
