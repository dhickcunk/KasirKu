package info.dhickcunk.kasirku.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import info.dhickcunk.kasirku.R;
import info.dhickcunk.kasirku.adapter.BarangListAdapter;
import info.dhickcunk.kasirku.model.DataBarang;
import info.dhickcunk.kasirku.model.Transaksi;
import info.dhickcunk.kasirku.service.APIService;
import info.dhickcunk.kasirku.service.KasirkuApi;
import info.dhickcunk.kasirku.utils.Session;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahTransaksiActivity extends BaseActivity {
    private Button scanBtn;
    private RecyclerView listBarang;
    LinearLayoutManager linearLayoutManager;
    TextView totalHargaText, jumlahBarangText;
    FloatingActionButton floatingActionButton;
    APIService apiService ;
    private String id;
    int total;
    ArrayList<Integer> arrayListIdBarang = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_transaksi);

        getSupportActionBar().setTitle("Tambah Transaksi");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        apiService = KasirkuApi.getClient().create(APIService.class);

        scanBtn = (Button)findViewById(R.id.scanBtn);
        jumlahBarangText = (TextView) findViewById(R.id.jumlahBarangText);
        totalHargaText = (TextView) findViewById(R.id.hargaTotalText);

        listBarang = (RecyclerView) findViewById(R.id.listBarang);
        linearLayoutManager = new LinearLayoutManager(this);
        listBarang.setLayoutManager(linearLayoutManager);

        SharedPreferences pref = TambahTransaksiActivity.this.getSharedPreferences(Session.PREF_NAME, 0);
        try {
            id = pref.getString("idUser", null);
        }catch (Exception e){
            Log.e("Kasir Activity", e.getMessage());
        }

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tanggal = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                String totalHarga = String.valueOf(total);

                if (totalHarga.isEmpty() || totalHarga.equalsIgnoreCase("0") || arrayListIdBarang.isEmpty() || arrayListIdBarang == null){

                }else{
                    Log.e("barang List", arrayListIdBarang+"");
                    insertTransaksi(tanggal, totalHarga, id, arrayListIdBarang);
                }
            }
        });

        scanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TambahTransaksiActivity.this, ScannerActivity.class);
                startActivityForResult(intent, 1);
            }
        });
    }

    public void setDataTransaksi(ArrayList<DataBarang> barangList){
        jumlahBarangText.setText(barangList.size()+"");
        total = 0;
        for (int i = 0; i < barangList.size(); i++){
            int hargaBarang = Integer.parseInt(barangList.get(i).getHarga());
            total = total + hargaBarang;
        }

        double amountDouble = (double) total;

        totalHargaText.setText("Rp. " + String.format("%,.0f", amountDouble));
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void insertTransaksi(String tglTransaksi, String totalHarga, String idKasir, ArrayList<Integer> idBarang) {
        apiService.insertTransaksi(tglTransaksi, totalHarga, idKasir, idBarang).enqueue(new Callback<Transaksi>() {
            @Override
            public void onResponse(Call<Transaksi> call, Response<Transaksi> response) {
                if (response.body().getSuccess().equalsIgnoreCase("1")){
                    Toast.makeText(TambahTransaksiActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                }else{
                    Toast.makeText(TambahTransaksiActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Transaksi> call, Throwable t) {
                Toast.makeText(TambahTransaksiActivity.this, ""+t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void isiArray(ArrayList<DataBarang> arr){
        for (int i = 0; i < arr.size(); i++){
            int idBarang = Integer.parseInt(arr.get(i).getIdBarang());
            arrayListIdBarang.add(idBarang);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                ArrayList<DataBarang> arr = (ArrayList<DataBarang>) data.getSerializableExtra("result");
                isiArray(arr);
                listBarang.setAdapter(new BarangListAdapter(arr, TambahTransaksiActivity.this));
                setDataTransaksi(arr);
            }
        }
    }
}
