package info.dhickcunk.kasirku.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;

import info.dhickcunk.kasirku.R;
import info.dhickcunk.kasirku.adapter.BarangListAdapter;
import info.dhickcunk.kasirku.model.DataTransaksi;

public class DetailTransaksiActivity extends BaseActivity {
    TextView tanggalTransaksiText;
    TextView totalHargaTransaksiText;
    TextView namaKasirText;
    RecyclerView listBarang;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_transaksi);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tanggalTransaksiText = (TextView) findViewById(R.id.tanggalTransaksi);
        totalHargaTransaksiText = (TextView) findViewById(R.id.totalHargaTransaksi);
        namaKasirText = (TextView) findViewById(R.id.namaKasirText);
        listBarang = (RecyclerView) findViewById(R.id.listBarang);

        linearLayoutManager = new LinearLayoutManager(this);
        listBarang.setLayoutManager(linearLayoutManager);

        Intent intent = getIntent();

        DataTransaksi dataTransaksi = (DataTransaksi) intent.getSerializableExtra("dataTransaksi");
        getSupportActionBar().setTitle("Transaksi "+dataTransaksi.getKodeTransaksi());
        tanggalTransaksiText.setText(dataTransaksi.getTglTransaksi());

        double amountDouble = Double.parseDouble(dataTransaksi.getTotalHarga());

        totalHargaTransaksiText.setText("Rp. " + String.format("%,.0f", amountDouble));
        namaKasirText.setText(dataTransaksi.getUser().getNamaUser());

        listBarang.setAdapter(new BarangListAdapter(dataTransaksi.getBarang(), DetailTransaksiActivity.this));
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
