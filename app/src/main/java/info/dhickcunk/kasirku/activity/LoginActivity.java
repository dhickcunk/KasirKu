package info.dhickcunk.kasirku.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import info.dhickcunk.kasirku.R;
import info.dhickcunk.kasirku.model.DataUser;
import info.dhickcunk.kasirku.model.Login;
import info.dhickcunk.kasirku.service.APIService;
import info.dhickcunk.kasirku.service.KasirkuApi;
import info.dhickcunk.kasirku.utils.Session;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {
    private static final String TAG = "Login Activity";
    private Button loginButton;
    private EditText usernameText, passwordText;
    private ProgressBar progressBar;
    private static final int ZBAR_CAMERA_PERMISSION = 1;
    APIService apiService ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        requestPermission();
        apiService = KasirkuApi.getClient().create(APIService.class);

        loginButton = (Button)findViewById(R.id.loginButton);
        usernameText = (EditText)findViewById(R.id.usernameText);
        passwordText = (EditText)findViewById(R.id.passwordText);
        progressBar = (ProgressBar)findViewById(R.id.progress);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameText.getText().toString().trim();
                String password = passwordText.getText().toString().trim();
                if(username.isEmpty() && password.isEmpty()){
                    Toast.makeText(LoginActivity.this, "Isi Password dan Text Terlebih Dahulu", Toast.LENGTH_SHORT).show();
                }else if (username.isEmpty()){
                    Toast.makeText(LoginActivity.this, "Username Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
                }else if(password.isEmpty()){
                    Toast.makeText(LoginActivity.this, "Password Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
                }else{
                    loginUser(username, password);
                }
            }
        });
    }

    //rewuest permission for android 6.0++ to use camera
    public void requestPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, ZBAR_CAMERA_PERMISSION);
        }
    }

    //request permisson handler
    @Override
    public void onRequestPermissionsResult(int requestCode,  String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ZBAR_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(this, "Akses Kamera Ditolak", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

    public void loginUser(String username, String password) {
        progressBar.setVisibility(View.VISIBLE);
        apiService.login(username, password).enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                Log.d(TAG, "Login : ");
                Login login = response.body();
                if (login.getSuccess().equalsIgnoreCase("0") ){
                    Toast.makeText(LoginActivity.this, login.getMessage(), Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                }else if (login.getSuccess().equalsIgnoreCase("1")){
                    progressBar.setVisibility(View.GONE);

                    DataUser dataUser = login.getData();

                    if (dataUser.getHakAkses().equalsIgnoreCase("admin")){
                        Intent intent = new Intent(LoginActivity.this, AdminActivity.class);
                        startActivity(intent);
                        finish();
                    }else if(dataUser.getHakAkses().equalsIgnoreCase("kasir")){
                        Intent intent = new Intent(LoginActivity.this, KasirActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    Toast.makeText(LoginActivity.this, "Selamat Datang "+ dataUser.getNamaUser(), Toast.LENGTH_SHORT).show();
                    Session.createLoginSession(LoginActivity.this, dataUser);
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Toast.makeText(LoginActivity.this, ""+t, Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
