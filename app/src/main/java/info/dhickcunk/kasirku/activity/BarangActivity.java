package info.dhickcunk.kasirku.activity;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import info.dhickcunk.kasirku.R;
import info.dhickcunk.kasirku.adapter.BarangLengkapAdapter;
import info.dhickcunk.kasirku.model.Barang;
import info.dhickcunk.kasirku.service.APIService;
import info.dhickcunk.kasirku.service.KasirkuApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BarangActivity extends BaseActivity implements SearchView.OnQueryTextListener {
    APIService apiService;
    RecyclerView listBarang;
    LinearLayoutManager linearLayoutManager;
    SwipeRefreshLayout mSwipeRefreshLayout;
    TextView kosongText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barang);

        //instasiasi apiService
        apiService = KasirkuApi.getClient().create(APIService.class);

        //Toolbar Configuration
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Barang");

        //find view by id
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.kontenRefreshView);
        listBarang = (RecyclerView) findViewById(R.id.listBarang);
        kosongText = (TextView) findViewById(R.id.kosongText);

        //set linearlayout manager to recycler view
        linearLayoutManager = new LinearLayoutManager(this);
        listBarang.setLayoutManager(linearLayoutManager);

        //set get all barang when swipe refresh
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAllBarang();
            }
        });

        //get all barang
        getAllBarang();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //inflate menu view
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_action, menu);

        //search stuff
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("Search");
        return true;
    }

    //back on toolbar
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //get all barang
    public void getAllBarang() {
        //set refreshing state for swipe refresh layout to true
        mSwipeRefreshLayout.setRefreshing(true);

        //calling getAllBarang
        apiService.getAllBarang().enqueue(new Callback<Barang>() {
            //if response success
            @Override
            public void onResponse(Call<Barang> call, Response<Barang> response) {
                Barang barang = response.body();

                //check json status
                if (barang.getSuccess().equalsIgnoreCase("1")) {

                    //check if barang is null or not
                    if (barang.getData().size() > 0) {
                        listBarang.setVisibility(View.VISIBLE);
                        kosongText.setVisibility(View.GONE);

                        //set adapter for recycler view
                        listBarang.setAdapter(new BarangLengkapAdapter(BarangActivity.this, barang.getData()));

                    }else{
                        listBarang.setVisibility(View.GONE);
                        kosongText.setVisibility(View.VISIBLE);
                    }
                }
                //set refreshing state for swipe refresh layout to false
                mSwipeRefreshLayout.setRefreshing(false);
            }

            //if response failed
            @Override
            public void onFailure(Call<Barang> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);

                //show the message to toast
                Toast.makeText(BarangActivity.this, "" + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    //search barang
    public void searchBarang(String searchQuery) {
        //set refreshing state to true
        mSwipeRefreshLayout.setRefreshing(true);

        //call search barang
        apiService.searchBarang(searchQuery).enqueue(new Callback<Barang>() {
            //on success
            @Override
            public void onResponse(Call<Barang> call, Response<Barang> response) {
                Barang barang = response.body();
                if (barang.getSuccess().equalsIgnoreCase("1")) {
                    if (barang.getData().size() > 0) {
                        listBarang.setVisibility(View.VISIBLE);
                        kosongText.setVisibility(View.GONE);

                        listBarang.setAdapter(new BarangLengkapAdapter(BarangActivity.this, barang.getData()));
                    }else{
                        listBarang.setVisibility(View.GONE);
                        kosongText.setVisibility(View.VISIBLE);
                    }
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }

            //on failed
            @Override
            public void onFailure(Call<Barang> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(BarangActivity.this, "" + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    //check for search bar if search submitted
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    //check for search bar if the text is changed
    @Override
    public boolean onQueryTextChange(final String newText) {
        //check for the search query if null or empty
        if (newText != null || !newText.trim().isEmpty() || newText.trim().equalsIgnoreCase("")) {
            //change refresh listener to search barang
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    searchBarang(newText);
                }
            });
            searchBarang(newText);
        } else {
            // if the query is empty then show all
            getAllBarang();
        }
        return false;
    }
}
