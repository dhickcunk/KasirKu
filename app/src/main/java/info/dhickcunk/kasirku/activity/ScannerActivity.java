package info.dhickcunk.kasirku.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import info.dhickcunk.kasirku.R;
import info.dhickcunk.kasirku.model.Barang;
import info.dhickcunk.kasirku.model.DataBarang;
import info.dhickcunk.kasirku.service.APIService;
import info.dhickcunk.kasirku.service.KasirkuApi;
import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScannerActivity extends BaseActivity implements ZBarScannerView.ResultHandler {
    private ZBarScannerView mScannerView;
    Toolbar toolbar;
    APIService apiService;
    FloatingActionButton floatingActionButton;

    ArrayList<DataBarang> arr = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);
        apiService = KasirkuApi.getClient().create(APIService.class);

        toolbar = (Toolbar)findViewById(R.id.toolbar_scanner);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Scan Barang");
        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra("result", arr);
                setResult(Activity.RESULT_OK, resultIntent);

                finish();
            }
        });

        ViewGroup contentFrame = (ViewGroup) findViewById(R.id.content_frame);
        mScannerView = new ZBarScannerView(this);
        mScannerView.setAutoFocus(true);
        contentFrame.addView(mScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        getBarangById(rawResult.getContents());
//        Toast.makeText(this, "Contents = " + rawResult.getContents() +
//                ", Format = " + rawResult.getBarcodeFormat().getName(), Toast.LENGTH_SHORT).show();
        // Note:
        // * Wait 2 seconds to resume the preview.
        // * On older devices continuously stopping and resuming camera preview can result in freezing the app.
        // * I don't know why this is the case but I don't have the time to figure out.
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mScannerView.resumeCameraPreview(ScannerActivity.this);
            }
        }, 2000);
    }

    public void getBarangById(String idBarang) {
        final ProgressDialog loadingProgress = progressDialog();
        loadingProgress.show();
        apiService.getBarangById(idBarang).enqueue(new Callback<Barang>() {
            @Override
            public void onResponse(Call<Barang> call, Response<Barang> response) {
                loadingProgress.dismiss();
                Barang barang = response.body();
                if (barang.getSuccess().equalsIgnoreCase("1")){
                    showDialogCostum(barang.getData().get(0));
                }else{
                    Toast.makeText(ScannerActivity.this, "Barang Tidak Ada", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Barang> call, Throwable t) {
                loadingProgress.dismiss();
                Toast.makeText(ScannerActivity.this, ""+t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public ProgressDialog progressDialog(){
        ProgressDialog progressDialog = new ProgressDialog(ScannerActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        return progressDialog;
    }

    public void showDialogCostum(final DataBarang dataBarang){
        // custom dialog
        final Dialog dialog = new Dialog(ScannerActivity.this);
        dialog.setContentView(R.layout.dialog_scan_barang);
        dialog.setTitle("Barang Ditemukan");
        dialog.setCanceledOnTouchOutside(false);
        // set the custom dialog components - text, image and button
        TextView namaBarangText = (TextView) dialog.findViewById(R.id.namaBarangText);
        TextView hargaBarangText = (TextView) dialog.findViewById(R.id.hargaBarangText);
        Button dialogButtonOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
        Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonNo);

        namaBarangText.setText(dataBarang.getNamaBarang());
        hargaBarangText.setText(dataBarang.getHarga());

        // if button is clicked, close the custom dialog
        dialogButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arr.add(dataBarang);
                Toast.makeText(ScannerActivity.this, "Barang ditambahkan ke keranjang belanja", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
