package info.dhickcunk.kasirku.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;

import info.dhickcunk.kasirku.R;
import info.dhickcunk.kasirku.utils.Session;

public class AdminActivity extends BaseActivity {
    private GridLayout menu_grid;
    boolean doubleBackToExitPressedOnce = false;
    ImageButton logoutBtn;
    TextView namaUserText;
    TextView hakAksesText;
    private String strIntent;
    CircularImageView profileImage;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        //Toolbar Configuration
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Set costum view for toolbar
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_action_bar, null);

        //set toolbar's title
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
        mTitleTextView.setText("Halaman Admin");

        //more toolbar configuration
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setCustomView(mCustomView);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        //get Session / Shared Preference
        SharedPreferences pref = AdminActivity.this.getSharedPreferences(Session.PREF_NAME, 0);
        String id = "";
        String namaUser = "";
        String hakAkses = "";
        String urlFoto = "";

        try {
            id = pref.getString("idUser", null);
            namaUser = pref.getString("namaUser", null);
            hakAkses = pref.getString("hakAkses", null);
            urlFoto = pref.getString("fotoUrl", null);
        }catch (Exception e){
            Log.e("Kasir Activity", e.getMessage());
        }

        //find view by id
        namaUserText = (TextView) findViewById(R.id.namaUserText);
        hakAksesText = (TextView) findViewById(R.id.hakAksesText);
        profileImage = (CircularImageView) findViewById(R.id.profileImage);

        //Set value from shared preference to view
        namaUserText.setText(namaUser);
        hakAksesText.setText(hakAkses);
        Glide.with(this)
                .load(urlFoto)
                .into(profileImage);

        //main menu
        menu_grid = (GridLayout) findViewById(R.id.menu_grid);
        int childCount = menu_grid.getChildCount();

        //onclick listener menu item
        for (int i= 0; i < childCount; i++){
            if (i == 0){
                LinearLayout container = (LinearLayout) menu_grid.getChildAt(i);
                container.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View view){
                        strIntent = "admin";
                        Intent intent = new Intent(AdminActivity.this, RiwayatTransaksiActivity.class);
                        intent.putExtra(RiwayatTransaksiActivity.KEY_DATA, strIntent);
                        startActivity(intent);
                    }
                });
            }else if (i == 1){
                LinearLayout container = (LinearLayout) menu_grid.getChildAt(i);
                container.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View view){
                        Intent intent = new Intent(AdminActivity.this, BarangActivity.class);
                        startActivity(intent);
                    }
                });
            }

        }

        //logout stuff
        logoutBtn = (ImageButton) findViewById(R.id.logoutBtn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(AdminActivity.this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Logout")
                    .setMessage("Apakah anda yakin ingin logout ?")
                    .setPositiveButton("Ya", new DialogInterface.OnClickListener(){
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Session.logout(AdminActivity.this);
                            Intent intent = new Intent(AdminActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    })
                    .setNegativeButton("Tidak", null)
                    .show();
            }
        });
    }

    //back 2 times to exit
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Klik kembali sekali lagi untuk keluar", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
