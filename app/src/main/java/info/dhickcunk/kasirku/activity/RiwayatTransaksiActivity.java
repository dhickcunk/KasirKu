package info.dhickcunk.kasirku.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import info.dhickcunk.kasirku.R;
import info.dhickcunk.kasirku.adapter.TransaksiListAdapter;
import info.dhickcunk.kasirku.model.Transaksi;
import info.dhickcunk.kasirku.service.APIService;
import info.dhickcunk.kasirku.service.KasirkuApi;
import info.dhickcunk.kasirku.utils.Session;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RiwayatTransaksiActivity extends BaseActivity implements SearchView.OnQueryTextListener {
    private RecyclerView listTransaksi;
    LinearLayoutManager linearLayoutManager;
    APIService apiService;
    TextView totalTransaksi;
    TextView totalUangText;
    public static String KEY_DATA = "data";
    int id = 0;
    String data;
    SwipeRefreshLayout mSwipeRefreshLayout;
    TextView kosongText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riwayat_transaksi);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Riwayat Transaksi");
        apiService = KasirkuApi.getClient().create(APIService.class);

        listTransaksi = (RecyclerView) findViewById(R.id.listTransaksi);
        totalTransaksi = (TextView) findViewById(R.id.totalTransaksiText);
        totalUangText = (TextView) findViewById(R.id.totalUangText);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.kontenRefreshView);
        kosongText = (TextView) findViewById(R.id.kosongText);

        linearLayoutManager = new LinearLayoutManager(this);
        listTransaksi.setLayoutManager(linearLayoutManager);

        //get Session / Shared Preference
        SharedPreferences pref = RiwayatTransaksiActivity.this.getSharedPreferences(Session.PREF_NAME, 0);
        try {
            id = Integer.parseInt(pref.getString("idUser", null));
        } catch (Exception e) {
            Log.e("Kasir Activity", e.getMessage());
        }

        data = getIntent().getStringExtra(KEY_DATA);

        if (data.equalsIgnoreCase("kasir")) {
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getTransaksiByKasir(id);
                }
            });
            getTransaksiByKasir(id);
        } else if (data.equalsIgnoreCase("admin")) {
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getAllTransaksi();
                }
            });
            getAllTransaksi();
        }
    }

    public void getAllTransaksi() {
        mSwipeRefreshLayout.setRefreshing(true);
        apiService.getAllTransaksi().enqueue(new Callback<Transaksi>() {
            @Override
            public void onResponse(Call<Transaksi> call, Response<Transaksi> response) {
                Transaksi transaksi = response.body();
                if (transaksi.getSuccess().equalsIgnoreCase("1")) {
                    if (transaksi.getData().size() > 0) {
                        totalTransaksi.setText("Total Transaksi : " + transaksi.getData().size());
                        int totalUang = 0;

                        for (int i = 0; i < transaksi.getData().size(); i++) {
                            int totalHarga = Integer.parseInt(transaksi.getData().get(i).getTotalHarga());
                            totalUang = totalUang + totalHarga;
                        }

                        double amountDouble = (double) totalUang;
                        totalUangText.setText("Rp. " + String.format("%,.0f", amountDouble));

                        listTransaksi.setVisibility(View.VISIBLE);
                        kosongText.setVisibility(View.GONE);

                        listTransaksi.setAdapter(new TransaksiListAdapter(RiwayatTransaksiActivity.this, transaksi.getData()));
                    }else{
                        listTransaksi.setVisibility(View.GONE);
                        kosongText.setVisibility(View.VISIBLE);
                    }
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<Transaksi> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(RiwayatTransaksiActivity.this, "" + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void searchAll(String searchQuery) {
        mSwipeRefreshLayout.setRefreshing(true);
        apiService.searchTransaksiAll(searchQuery).enqueue(new Callback<Transaksi>() {
            @Override
            public void onResponse(Call<Transaksi> call, Response<Transaksi> response) {
                Transaksi transaksi = response.body();
                if (transaksi.getSuccess().equalsIgnoreCase("1")) {
                    if (transaksi.getData().size() > 0) {
                        totalTransaksi.setText("Total Transaksi : " + transaksi.getData().size());
                        int totalUang = 0;

                        for (int i = 0; i < transaksi.getData().size(); i++) {
                            int totalHarga = Integer.parseInt(transaksi.getData().get(i).getTotalHarga());
                            totalUang = totalUang + totalHarga;
                        }

                        double amountDouble = (double) totalUang;
                        totalUangText.setText("Rp. " + String.format("%,.0f", amountDouble));

                        listTransaksi.setVisibility(View.VISIBLE);
                        kosongText.setVisibility(View.GONE);

                        listTransaksi.setAdapter(new TransaksiListAdapter(RiwayatTransaksiActivity.this, transaksi.getData()));
                    }else {
                        listTransaksi.setVisibility(View.GONE);
                        kosongText.setVisibility(View.VISIBLE);
                    }
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<Transaksi> call, Throwable t) {
                Toast.makeText(RiwayatTransaksiActivity.this, "" + t, Toast.LENGTH_SHORT).show();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public void getTransaksiByKasir(int idKasir) {
        mSwipeRefreshLayout.setRefreshing(true);
        apiService.getTransaksiByKasir(idKasir).enqueue(new Callback<Transaksi>() {
            @Override
            public void onResponse(Call<Transaksi> call, Response<Transaksi> response) {
                Transaksi transaksi = response.body();
                if (transaksi.getSuccess().equalsIgnoreCase("1")) {
                    if (transaksi.getData().size() > 0) {
                        totalTransaksi.setText("Total Transaksi : " + transaksi.getData().size());
                        int totalUang = 0;

                        for (int i = 0; i < transaksi.getData().size(); i++) {
                            int totalHarga = Integer.parseInt(transaksi.getData().get(i).getTotalHarga());
                            totalUang = totalUang + totalHarga;
                        }
                        double amountDouble = (double) totalUang;
                        totalUangText.setText("Rp. " + String.format("%,.0f", amountDouble));

                        listTransaksi.setVisibility(View.VISIBLE);
                        kosongText.setVisibility(View.GONE);

                        listTransaksi.setAdapter(new TransaksiListAdapter(RiwayatTransaksiActivity.this, transaksi.getData()));
                    }else{
                        listTransaksi.setVisibility(View.GONE);
                        kosongText.setVisibility(View.VISIBLE);
                    }
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<Transaksi> call, Throwable t) {
                Toast.makeText(RiwayatTransaksiActivity.this, "" + t, Toast.LENGTH_SHORT).show();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public void searchByKasir(int idKasir, String searchQuery) {
        mSwipeRefreshLayout.setRefreshing(true);
        apiService.searchByKasir(idKasir, searchQuery).enqueue(new Callback<Transaksi>() {
            @Override
            public void onResponse(Call<Transaksi> call, Response<Transaksi> response) {
                Transaksi transaksi = response.body();
                if (transaksi.getSuccess().equalsIgnoreCase("1")) {
                    if (transaksi.getData().size() > 0) {
                        totalTransaksi.setText("Total Transaksi : " + transaksi.getData().size());
                        int totalUang = 0;

                        for (int i = 0; i < transaksi.getData().size(); i++) {
                            int totalHarga = Integer.parseInt(transaksi.getData().get(i).getTotalHarga());
                            totalUang = totalUang + totalHarga;
                        }

                        double amountDouble = (double) totalUang;
                        totalUangText.setText("Rp. " + String.format("%,.0f", amountDouble));

                        listTransaksi.setVisibility(View.VISIBLE);
                        kosongText.setVisibility(View.GONE);

                        listTransaksi.setAdapter(new TransaksiListAdapter(RiwayatTransaksiActivity.this, transaksi.getData()));
                    }else{
                        listTransaksi.setVisibility(View.GONE);
                        kosongText.setVisibility(View.VISIBLE);
                    }
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<Transaksi> call, Throwable t) {
                Toast.makeText(RiwayatTransaksiActivity.this, "" + t, Toast.LENGTH_SHORT).show();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_action, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("Search");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(final String newText) {
        if (newText != null || !newText.trim().isEmpty() || newText.trim().equalsIgnoreCase("")) {
            if (data.equalsIgnoreCase("kasir")) {
                mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        searchByKasir(id, newText);
                    }
                });
                searchByKasir(id, newText);
            } else if (data.equalsIgnoreCase("admin")) {
                mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        searchAll(newText);
                    }
                });
                searchAll(newText);
            }
        } else {
            if (data.equalsIgnoreCase("kasir")) {
                getTransaksiByKasir(id);
            } else if (data.equalsIgnoreCase("admin")) {
                getAllTransaksi();
            }
        }
        return false;
    }
}